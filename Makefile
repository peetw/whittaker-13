# Utilities
LATEXMK = latexmk
GNUPLOT = gnuplot

# Main TeX and PDF files
TEX_SRC := $(shell find . -type f -name "*.tex")
MAIN_SRC = whittaker-13.tex
MAIN_PDF := $(MAIN_SRC:.tex=.pdf)

# BibTeX files
BIB_SRC := $(MAIN_SRC:.tex=.bib) $(MAIN_SRC:.tex=.bst)

# Gnuplot files
IMG_DIR = images
GNU_DIR = gnuplot
GNU_SRC := $(shell find . -type f -name "*.plt")
GNU_EPS := $(shell grep -H "set out" $(GNU_SRC) | sed 's/$(GNU_DIR)\/.*\($(IMG_DIR)\/.*\.eps\).*/\1/')
GNU_PDF := $(GNU_EPS:.eps=-eps-converted-to.pdf)
GNU_DEP = .depend

# Submission directory and files
SUB_DIR = submission
SUB_PS := $(MAIN_SRC:.tex=.ps)

# Non-file targets
.PHONY: all submission clean clean-pdf clean-extra clean-all

# Default target
all: $(MAIN_PDF)

# Compile main PDF
$(MAIN_PDF): $(TEX_SRC) $(BIB_SRC) $(GNU_EPS)
	$(LATEXMK) -f -silent -pdf -pdflatex="pdflatex --shell-escape %O %S" $(MAIN_SRC)

# Determine gnuplot EPS dependencies
$(GNU_DEP): $(GNU_SRC)
	@echo Determining gnuplot EPS dependencies...
	@grep -H "set out" $^ > $(GNU_DEP)
	@sed -i 's/\(.*\)\($(GNU_DIR)\/.*\.plt\).*\($(IMG_DIR)\/.*\.eps\).*/\1\3: \1\2/' $(GNU_DEP)

-include $(GNU_DEP)

# Generate gnuplot EPS files
$(GNU_EPS):
	@mkdir -p $(IMG_DIR)
	cd $(shell dirname $<) && $(GNUPLOT) $(shell basename $<) 2>/dev/null

# Organize files ready for submission to journal
submission: $(SUB_PS)
	mkdir -p $(SUB_DIR)
	cp $(SUB_PS) $(GNU_EPS) $(SUB_DIR)

# Compile PS file for submission
$(SUB_PS): $(TEX_SRC) $(GNU_EPS)
	$(LATEXMK) -f -silent -ps $(MAIN_SRC)

# Remove intermediary LaTeX files
clean: clean-extra
	$(LATEXMK) -silent -c $(MAIN_SRC)

# Remove all intermediary LaTeX files, including PDF, DVI and PS
clean-pdf: clean-extra
	$(LATEXMK) -silent -C $(MAIN_SRC)

# Remove intermediary LaTeX files that latexmk misses
clean-extra:
	$(RM) $(MAIN_SRC:.tex=.bbl)
	$(RM) $(MAIN_SRC:.tex=.fff)
	$(RM) $(MAIN_SRC:.tex=.ttt)

# Remove all generated files
clean-all: clean-pdf
	$(RM) $(GNU_EPS)
	$(RM) $(GNU_PDF)
	$(RM) $(GNU_DEP)
	$(RM) -r $(SUB_DIR)
