%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% METHODOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sec:Method}

\noindent{}The experimental data presented in this paper were obtained from drag force tests undertaken on fully submerged trees at the Canal de Experiencias Hidrodinamicas de El Pardo (\mbox{CEHIPAR}), Madrid. The data were collected over a four week period during the spring of 2008 by members of Cardiff University in collaboration with Technische Universit\"{a}t Braunschweig, Germany, and the University of Natural Resources and Life Sciences, Vienna. In total, twenty-one full-scale trees were tested, including five \textit{Alnus glutinosa} (\textit{A1}-\textit{5}), four \textit{Populus nigra} (\textit{P1}-\textit{4}), and twelve \textit{Salix alba} (\textit{S1}-\textit{12}) specimens. This provides a relatively extensive and unique data set upon which the present work is based.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Experimental Facilities and Procedure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent{}The facilities at CEHIPAR include a calm water ship towing tank, with the following dimensions: 320~m long, 12.5~m wide and 6.5~m deep. A towing carriage was mounted on rails running the length of the canal and the speed at which it travelled in either direction could be set with an accuracy of 0.001~\mps. The towing carriage had a maximum velocity of 10~\mps and a maximum acceleration of 1~\acc. A dynamometer, consisting of five separate load cells, was suspended underneath the carriage in order to measure the forces and moments in the three Cartesian axes at a rate of 10 Hz and with an accuracy of 0.0098~N.

The tree specimens were attached upside down to the dynamometer so that they were fully submerged and were towed with velocities ranging from 0.125~\mps to 6~\mps. Although flood flows rarely reach such high velocities in practice, the large range provides important test cases for any proposed models. The use of fully submerged conditions was necessary due to the experimental facilities, but provides insight into the nature of the deformation of full-scale trees in extreme flow conditions.

To ensure repeatability of results each experiment was carried out twice, \ie specimens were towed in both directions along the towing tank by the carriage. After each run the trees were rotated through 180 degrees in the dynamometer. Sufficient time (5-10 minutes) was also reserved between runs to allow any surface waves or eddies to dissipate before towing again.

The majority of trees were tested first in a foliated and then in a defoliated state. However, during preliminary tests it was observed that the trees exhibited apparent signs of inelastic deformation. It was therefore decided that the trees would be exposed to lower towing velocities while in their foliated state in order to minimise the impact on the subsequent defoliated tests. Additionally, a number of specimens were selected for further testing based on their branch morphology. These trees were only tested in a foliated state and then cut into their constituent stems, which were then tested separately in foliated and defoliated states. This allowed the contribution of the tree's branches to its overall drag force to be investigated. However, it also meant that the original main specimen could not be tested in a defoliated state since it was no longer whole.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Quantification of Tree Physical Properties}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sec:Physical-properties}

\noindent{}The tested trees were sourced at two week intervals from a local floodplain woodland site, rather than an artificial nursery, and were selected to cover a broad range of growth habits \citep{Weissteiner-09}. All trees were brought to the laboratory within five hours and kept with their stems submerged in the water of the canal. From visual inspection, the trees remained fresh and leaf growth was observed to continue for approximately two weeks before obvious signs of decay occurred, at which point the specimens were discarded.

Prior to the towing tank tests, the specimens' physical dimensions were measured and recorded. This included the height of the specimen and the diameter of the main stem at each quartile height (\ie basal $d_0$, first-quartile $d_{25}$, mid-stem $d_{50}$, and third-quartile $d_{75}$).

After a specimen had been tested in a foliated state, the foliage was removed and the fresh, or wet leaf mass was recorded using a scale with an accuracy of 0.1~g. The leaf volume was determined by immersing the collected foliage in a known volume of water and recording the volume of water displaced. The foliage was then allowed to dry in a warm oven for at least 24 hours so that the dry leaf mass could be noted. Once a defoliated specimen had finished being tested, it was cut into sections and the wet mass, volume and dry mass of the wood was recorded using the same procedure as for the leaf matter.

The recorded physical properties are summarised in Tab.~\ref{tab:Specimen-properties} for the \Alnus, \Populus and \Salix specimens. The leaf to wood ratios for the specimens' mass and volume were calculated by dividing the leaf mass and volume by the corresponding wood mass or volume. It should be noted that the experimental time at the \mbox{CEHIPAR} facilities was limited so that not all of the properties could be determined for every specimen. The sub-branches of the specimens that contained multiple main stems are denoted using the suffix `B' combined with the number of the respective sub-branch.

\begin{table}[htbp]
\caption{Measured physical properties of the \Alnus (A), \Populus (P) and \Salix (S) specimens. A dash (-) indicates data not recorded. The suffix `B' denotes a branch cut from the specimen with the same prefix.}
\centering
\begin{footnotesize}
\begin{tabular}
{m{0.075\columnwidth}
>{\centering}m{0.055\columnwidth}
>{\centering}m{0.08\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.075\columnwidth}}
\toprule
 & \multicolumn{5}{c}{Dimensions} & \multicolumn{3}{c}{Leaf to wood ratio} \tabularnewline
\cmidrule(lr){2-6} \cmidrule(lr){7-9}
Specimen & Height\\(m) & $d_0$ diam.\\(mm) & $d_{25}$ diam.\\(mm) & $d_{50}$ diam.\\(mm) & $d_{75}$ diam.\\(mm) & Wet mass\\(\%) & Dry mass\\(\%) & Volume\\(\%) \tabularnewline \midrule
A1 & 2.45 & 58 & 29 & 25 & 11 & 7.0 & - & 15.9 \tabularnewline
A2 & 3.6 & 35 & 33 & 18 & 9 & 7.9 & 3.7 & 10.1 \tabularnewline
A3 & 2.6 & 28 & 21 & 16 & 5 & - & - & - \tabularnewline
A4 & 2.4 & 27 & 22 & 16 & 8 & 12.3 & 5.7 & 16.8 \tabularnewline
A5 & 1.8 & 28 & 20 & 12 & 8 & 8.3 & - & 9.8 \tabularnewline \midrule
Average & 2.57 & 35.4 & 25.0 & 17.5 & 8.0 & 8.9 & 4.7 & 13.1 \tabularnewline \midrule
P1 & 2.68 & 35 & 18 & 12 & 8 & - & - & - \tabularnewline
P2 & 3.77 & 37 & 23 & 21 & 8 & 8.7 & 3.9 & - \tabularnewline
P2B1 & 3.77 & 37 & 23 & 21 & 8 & 8.9 & 3.9 & - \tabularnewline
P2B2 & 2.5 & - & 22 & 20 & 8 & 8.4 & 3.8 & 21.6 \tabularnewline
P3 & 2.6 & 23 & 20 & 16 & 7 & 9.3 & 4.2 & 10.6 \tabularnewline
P4 & 3.9 & 35 & 29 & 22 & 16 & 9.0 & 6.7 & - \tabularnewline
P4B1 & 2.3 & 22 & - & 16 & - & 3.8 & 2.8 & 5.1 \tabularnewline
P4B2 & 1.8 & 16 & 15 & 10 & 6 & 41.2 & 30.3 & - \tabularnewline \midrule
Average & 2.92 & 29.1 & 21.4 & 17.3 & 8.8 & 12.8 & 7.9 & 12.4 \tabularnewline \midrule
S1 & 2.10 & 29 & 25 & 17 & 8 & 6.6 & 11.6 & 4.4 \tabularnewline
S2 & 2.40 & 26 & 21 & 11 & 3 & 5.0 & - & 7.7 \tabularnewline
S3 & 3.95 & 44 & 40 & 28 & 10 & 5.9 & - & 9.3 \tabularnewline
S4 & 2.00 & 23 & 16 & 9 & 5 & 18.5 & - & - \tabularnewline
S5 & 3.60 & 47 & 14 & 18 & 9 & 25.3 & - & 23.6 \tabularnewline
S5B1 & 3.60 & 47 & 14 & 18 & 9 & 23.2 & 12.7 & 22.0 \tabularnewline
S5B2 & 3.60 & 47 & 14 & 11 & 8 & 27.8 & - & 25.4 \tabularnewline
S6 & 3.20 & 25 & 12 & 9 & 5 & 36.3 & 21.4 & 47.0 \tabularnewline
S6B1 & 3.20 & 25 & 12 & 9 & 5 & 32.2 & 18.9 & 47.2 \tabularnewline
S6B2 & 2.80 & - & 16 & 8 & 4 & 42.4 & 24.9 & 46.7 \tabularnewline
S7 & 2.30 & 31 & 19 & 15 & 8 & 8.6 & 4.7 & 13.4 \tabularnewline
S7B1 & 2.30 & 31 & 19 & 15 & 8 & 10.8 & - & 19.0 \tabularnewline
S7B2 & 2.30 & 31 & 21 & 18 & 8 & 9.0 & - & 14.3 \tabularnewline
S7B3 & 2.18 & 31 & 19 & 14 & 7 & 6.5 & - & 8.3 \tabularnewline
S8 & 3.00 & 20 & 17 & 13 & 5 & 30.9 & 26.6 & - \tabularnewline
S9 & 3.60 & 29 & 23 & 14 & 7 & 30.6 & - & 33.1 \tabularnewline
S10 & 3.24 & 33 & 31 & 22 & 14 & 14.2 & 7.5 & 19.7 \tabularnewline
S11 & 3.50 & 26 & 18 & 11 & 7 & 16.4 & 14.7 & 16.4 \tabularnewline
S12 & 4.10 & 29 & 21 & 16 & 7 & 8.8 & 9.5 & 8.9 \tabularnewline \midrule
Average & 3.00 & 32.1 & 19.5 & 14.6 & 7.2 & 18.9 & 15.2 & 21.5 \tabularnewline \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Specimen-properties}
\end{table}

In addition to the above properties, tests were also carried out to determine the specimens' flexural rigidity $EI$ and modulus of elasticity $E$. Each tree was fixed at its base to the edge of a secure table using G-clamps. Weights were then attached to the horizontal tree at either the first-quartile or mid-stem position and the resulting vertical deflection recorded. The weights were increased incrementally from 0.2~kg to 13~kg. The properties derived from placing weights at each position are denoted using a subscript suffix of 25 or 50 respectively. For example, $EI_{50}$ would indicate the flexural rigidity calculated from placing weights at the mid-stem position.

Although classical beam bending theory (\eg \citealp{Coates-90}) is derived for small deflections and based on the assumption that the beam is uniform in cross-section and has linear elasticity, it is often used in biomechanical studies of natural vegetation \citep{Chen-11, Stone-11}. Since the assumptions may not hold for natural vegetation (non-constant thickness, large deformations, differing properties of young and old wood, \etc) an averaging method was employed. For each weight applied to the tree, the corresponding flexural rigidity value was calculated. Once the deflections became too large, no more weights were added and the previous flexural rigidity values were averaged to give one result. While this method provides reasonably accurate approximations, a more detailed or rigorous procedure was outside the scope of this project. Representing the specimens as a beam of length $L$ that is fixed at one end and has a concentrated load $P$ at a distance $l$ from the free end, the deflection $\delta$ can be expressed:
%
\begin{equation}
\delta = \frac{P}{3 E I} (L-l)^3
\label{eq:Beam-deflection}
\end{equation}
%
where $I = \frac{\pi r^4}{4}$ is the second moment of area for a circular cross-section of radius $r$.

Following the above procedure, flexural rigidity properties were calculated from placing weights at both the first-quartile ($EI_{25}$) and mid-stem ($EI_{50}$) heights. These are summarised in Tab.~\ref{tab:Salix-elasticity} for the \Salix specimens, along with the equivalent modulus of elasticity values. Due to the tapering thickness of the trees' main stems, the average radius $r$ of the stem between the base and the point of loading was used to determine $I$. The modulus of elasticity values reported in Tab.~\ref{tab:Salix-elasticity} are consistent with those given in the literature for willow specimens \citep{Green-99}.

\begin{table}[htbp]
\caption{Flexural rigidity and modulus of elasticity values for the \Salix specimens. A dash (-) indicates data not recorded.}
\centering
\begin{footnotesize}
\begin{tabular}
{m{0.075\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.09\columnwidth}
>{\centering}m{0.15\columnwidth}
>{\centering}m{0.15\columnwidth}}
\toprule
Specimen & $EI_{25}$\\(\bstiff) & $EI_{50}$\\(\bstiff) & $E_{25}$\\(\ymod) & $E_{50}$\\(\ymod)\tabularnewline \midrule
S1 & - & - & - & - \tabularnewline
S2 & 119 & 141 & $7.58 \times 10^{9}$ & $2.29 \times 10^{10}$ \tabularnewline
S3 & 367 & 559 & $2.34 \times 10^{9}$ & $6.81 \times 10^{9}$ \tabularnewline
S4 & 31 & 42 & $4.33 \times 10^{9}$ & $1.38 \times 10^{10}$ \tabularnewline
S5 & - & - & - & - \tabularnewline
S5B1 & 65 & 56 & $1.50 \times 10^{9}$ & $1.01 \times 10^{9}$ \tabularnewline
S5B2 & 157 & 51 & $3.79 \times 10^{9}$ & $1.44 \times 10^{9}$ \tabularnewline
S6 & - & - & - & - \tabularnewline
S6B1 & 20 & 18 & $3.40 \times 10^{9}$ & $4.06 \times 10^{9}$ \tabularnewline
S6B2 & 47 & 20 & - & - \tabularnewline
S7 & - & - & - & - \tabularnewline
S7B1 & 85 & 72 & $4.14 \times 10^{9}$ & $5.07 \times 10^{9}$ \tabularnewline
S7B2 & 84 & 48 & $3.68 \times 10^{9}$ & $2.66 \times 10^{9}$ \tabularnewline
S7B3 & - & - & - & - \tabularnewline
S8 & 35 & 40 & $5.89 \times 10^{9}$ & $1.06 \times 10^{10}$ \tabularnewline
S9 & - & - & - & - \tabularnewline
S10 & 258 & 349 & $4.89 \times 10^{9}$ & $1.24 \times 10^{10}$ \tabularnewline
S11 & 64 & 52 & $5.62 \times 10^{9}$ & $8.70 \times 10^{9}$ \tabularnewline
S12 & 141 & 174 & $7.45 \times 10^{9}$ & $1.40 \times 10^{10}$ \tabularnewline \midrule
Average & 113 & 125 & $4.55 \times 10^{9}$ & $8.62 \times 10^{9}$ \tabularnewline \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Salix-elasticity}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dimensional Analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sec:Cauchy-theory}

\noindent{}As previously stated in Section~\ref{sec:Intro}, the drag force response of full-scale flexible vegetation is governed by an array of complex interactions. In order to parameterise this response in terms of the vegetation's physical properties we shall assume that the drag force $F$ is dependent on the velocity $U$, density $\rho$ and kinematic viscosity $\nu$ of the fluid, along with the vegetation's projected area \Ap and modulus of elasticity $E$. According to the $\Pi$ theorem of \citet{Buckingham-14}, this system can be described by three dimensionless numbers. Here, we choose the drag coefficient, Reynolds number and the Cauchy number:
%
\begin{equation}
C_d = \frac{2 F}{\rho A_p U^2}; \qquad \Reyn = \frac{A^{1/2}_{p} U}{\nu}; \qquad \Cy = \frac{\rho U^2}{E}
\label{eq:Dimensional-analysis}
\end{equation}

This implies that the drag coefficient is dependent on both the Reynolds number and the Cauchy number. For a fully stiff object, the Cauchy number will be negligible and therefore there will be no deformation under loading \citep{Blevins-90, Cermak-98, Chakrabarti-02}. This leads to the well known observation that for rigid objects, such as plates and cylinders, the drag coefficient is a function of the Reynolds number only. Further, for flows where the Reynolds number effect is essentially constant, the above set of parameters form the classical drag equation (\ref{eq:Classical-drag}).

The Cauchy number defined above, however, does not take into account the cross-sectional area or `slenderness' of an object and, as such, cannot accurately represent the compressibility of vegetation \citep{Langre-08}. Therefore, the Cauchy number is re-defined to incorporate the flexural rigidity $EI$:
%
\begin{equation}
\Cy = \frac{\rho U^2 V H}{EI}
\label{eq:Cauchy}
\end{equation}
%
where $V$ is the object's volume and $H$ is the object's undisturbed height in still air. While this `vegetative' Cauchy number has not been investigated for full-scale flexible vegetation before, \citet{Luhar-11} predicted the drag force and posture of idealised seagrasses and marine macroalgae with high accuracy using an approach based on the vegetations' Cauchy number and buoyancy. \citet{Gosselin-10b} and \citet{Gosselin-11} also described the reconfiguration in terms of the Cauchy number in drag force studies on flexible plates and fibres.

If Reynolds number effects are assumed to be negligible, it follows from Eq.~(\ref{eq:Dimensional-analysis}) that, for flexible vegetation, the variation in characteristic drag coefficient \CdAp is determined by the Cauchy number \citep{Langre-08}, \ie $C_d A_p = f(\Cy)$. For our model, the dimensionless Cauchy number in Eq.~(\ref{eq:Cauchy}) is rearranged and raised to an arbitrary power. This power is then identical to the Vogel exponent in Eq.~(\ref{eq:Vogel-exponent}) and the drag force can be expressed:
%
\begin{equation}
F = \frac{1}{2} \rho K \left(U \sqrt{\frac{\rho V H}{E I}}\right)^\psi U^2
\label{eq:Cauchy-drag}
\end{equation}
%
where $K$ is a coefficient that corresponds to some initial \CdAp value and thus has the units of m\sps{2}.

Physically, Eq.~(\ref{eq:Cauchy-drag}) relates the reduction in projected area and drag coefficient experienced by flexible objects in aero- and hydrodynamic flows, to the compressibility of the object. The magnitude of the Vogel exponent accounts for the rate and nature of that reduction. For example, the characteristic drag coefficient of an object with $\psi = -0.7$ will decrease quicker with velocity, compared to a similar object where $\psi = -0.5$. The relationship is valid for both flexible and rigid objects since when $\psi = 0$, it returns to the classical drag equation (\ref{eq:Classical-drag}). This enables the specimens in the present study to be categorised based on their Vogel exponent and the value of the coefficient $K$.
