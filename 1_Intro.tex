%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sec:Intro}

\noindent{}In many river systems, forests form the natural vegetation inhabiting the floodplain zones, with the range of plant species at a given location being influenced by the position within the river basin \citep{Tabacchi-96}. However, due to commercial pressures and river management activities, floodplain forests are now considered a threatened habitat in many European countries \citep{UNEP-00, Hughes-03}. In order to combat this, there has been a resurgence in the conservation and restoration of riparian woodland in recent years.

The hydraulic impact of such vegetation must be properly considered since that vegetation will generally modify the sediment transport capabilities of the adjacent river and increase the flow resistance on the floodplain itself \citep{Yen-02}. This is particularly important when using numerical models to assess the flood risk of a particular catchment, where the selection of a suitable roughness parameter can significantly affect the accuracy of the simulation.

The most widely utilised approach for representing the hydraulic effect of vegetation has been through the use of friction coefficients such as Manning's $n$ or Darcy-Weisbach's $f$. These combine multiple roughness effects, such as bed friction, vegetative drag, effects of channel cross-section, \etc, into a single term by utilising the superposition principle (\eg \citealp{Cowan-56, Petryk-75, Yen-02}). Recent studies, however, have criticised the use of the dimensional Manning's $n$ coefficient \citep{Ferguson-10} and instead favoured the use of the dimensionless Darcy-Weisbach friction factor $f$ (\eg \citealp{Jarvela-02a, Fathi-07}). Focusing on flow situations where only bed friction and vegetative drag are of significance, the total friction factor can be decomposed into a bed roughness factor $f'$ and a form resistance factor $f''$ according to the linear superposition principle, \ie $f = f' + f''$. In investigations with rigid vegetation and cylinders (\eg \citealp{Li-73, Huthoff-07}), the form factor $f''$ is related to the spatially-averaged drag force $\langle F \rangle$ via:
%
\begin{equation}
f'' = \frac{8 \langle F \rangle}{s_x s_y \rho U^2}
\label{eq:Form-factor}
\end{equation}
%
where $s_x$ and $s_y$ are the longitudinal and lateral spacing of the roughness elements; $\rho$ is the density of the fluid; and $U$ is the reference velocity, typically taken as the free stream velocity. The classical drag force equation for a single rigid roughness element \citep{Douglas-05} can be expressed as:
%
\begin{equation}
F = \frac{1}{2} \rho C_{d} A_{p} U^{2}
\label{eq:Classical-drag}
\end{equation}
%
where \Cd is the dimensionless drag coefficient and \Ap is the frontal projected area of the body.

There have been numerous tests on isolated cylinders and other standard objects, and the variation in drag coefficient with flow Reynolds number \Reyn is well documented for such cases (\eg \citealp{Schlichting-79}). At higher Reynolds numbers, the drag coefficient is essentially constant and Eq.~(\ref{eq:Classical-drag}) describes a quadratic relationship between drag force and velocity. Indeed, this quadratic drag force relationship has been found to be applicable for artificially stiffened vegetation (\eg \citealp{Aberle-12}). However, there are a number of difficulties in applying the classical drag equation to flexible vegetation. This is because as the hydrodynamic or aerodynamic loading is increased, the vegetation tends to reconfigure and streamline in order to reduce the magnitude of the drag force it experiences \citep{Vogel-84, Aberle-13}.

As a result, it is impractical to evaluate the projected area \Ap at every required velocity and, at present, no computational approach exists to calculate it as a function of the flow conditions. Furthermore, the drag coefficient for a tree-like object is currently impossible to determine precisely \textit{a priori} and is likely to be dependent on both velocity and tree morphology \citep{Mayhead-73, Vogel-89, Gaylord-94, Armanini-05, Boller-06, Dittrich-12}.

Moreover, both \Ap and \Cd also depend on the presence and quantity of foliage, which has been identified to contribute significantly to drag (\eg \citealp{Vogel-84, Armanini-05, Wilson-08}). Finally, the proportions of form and skin-friction drag with respect to the overall drag will also vary as the tree streamlines into a more aerodynamic shape \citep{Shames-03, Sand-Jensen-08b}. It is claimed that this reconfiguration is an essential mechanism employed by vegetation in order to reduce the stress induced by an external flow \citep{Harder-04, Nikora-10}.

The issues detailed above have been highlighted by a number of atmospheric and hydraulic studies in which the measured force-velocity relationship for flexible vegetation deviates away from a quadratic response, even at high Reynolds numbers (see, for example, \citealp{Fathi-97, Oplatka-98, Jarvela-04a, Sand-Jensen-08a, Wilson-08, Schoneboom-11}). Some authors have suggested that the force-velocity relationship for flexible vegetation may, in fact, be characterised as linear at certain velocities \citep{Vischer-98, Cullen-05, Xavier-10}.

The extent to which the vegetation reconfigures and the drag force deviates from the squared velocity relationship (Eq.~\ref{eq:Classical-drag}), has often been expressed in terms of a Vogel exponent \E. The idea was first introduced by \citet{Vogel-84} and later studied by, for example, \citet{Gaylord-94} and \citet{Gosselin-11}. The Vogel exponent was originally devised in order to investigate the drag force response of vegetation in regions of large deformation, \ie above a certain threshold velocity. In general terms and not considering the dependency of \Cd on \Reyn, the Vogel exponent modifies the power to which the velocity is raised in the classical drag formula, so that:
%
\begin{equation}
F \sim U^{2+\psi}
\label{eq:Vogel-exponent}
\end{equation}
%
with most reported values of \E ranging between $-0.2$ and $-1.2$ \citep{Langre-12}. For a rigid object or plant $\psi = 0$ and Eq.~(\ref{eq:Vogel-exponent}) returns to a classical squared relation, while a value of $\psi = -1$ would indicate a linear force-velocity relationship. \citet{Alben-02} and \citet{Langre-08} showed that for flexible fibres, scaling predicts an exponent of $\psi = -2/3$, with similar values reported for individual leaves \citep{Albayrak-12}.

The Vogel exponent concept is also represented in the work of \citet{Jarvela-04a}, who proposed that the form resistance factor $f''$ for just submerged vegetation is dependent on the leaf area index (LAI) and certain species-specific parameters:
%
\begin{equation}
f'' = 4 C_{d\chi} LAI \left( \frac{U}{U_{\chi}} \right)^\chi
\label{eq:Chi-value}
\end{equation}
%
where $C_{d\chi}$ is a species-specific drag coefficient; \X is also unique to a particular species and accounts for the reconfiguration of flexible vegetation; and $U_{\chi}$ is a scaling value included to ensure dimensional homogeneity, equal to the lowest velocity used in determining \X. It can be found from rearranging Eqs.~(\ref{eq:Form-factor}) and (\ref{eq:Chi-value}) that the \X value is equivalent to the Vogel exponent, \ie $F \sim U^{2+\chi}$ \citep{Aberle-13}.

As such, the quadratic force-velocity relationship of the unmodified classical drag equation is not always suitable for modelling the hydrodynamic force exerted on flexible vegetation. This paper is one of the first to seek to address this issue for full-scale submerged floodplain woodland, by attempting to link the physical properties of the vegetation to the force-velocity response. Firstly, high resolution drag force and physical property data from a large-scale laboratory investigation are introduced. A drag-velocity model incorporating a vegetative Cauchy number, which accounts for the reconfiguration of the vegetation under hydrodynamic loading, is then proposed and developed using the experimental data. Finally, the correlation of the model parameters with the specimens' physical properties are discussed.