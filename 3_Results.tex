%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULTS AND DISCUSSION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent{}In this section the experimental data and results from the subsequent model analysis are presented and discussed. In total, including the sub-branches, drag tests were carried out on 32 foliated and 25 defoliated specimens. The data for each species (\Alnus, \Populus, and \Salix) are first considered as a whole when discussing general trends and then as separate species when addressing more specific relationships based on the specimens' properties. The reason for this is that while a tree's morphology and biomechanics may be influenced by the local conditions in which it grows \citep{Haines-04}, recent studies have shown that there are inter-species differences in the drag force response \citep{Kouwen-00, Jarvela-04a, Aberle-13}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Drag Forces}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent{}If the streamwise drag force is divided by the square of the towing velocity ($F / U^2$), the resulting `speed-specific' drag \citep{Vogel-84} may be plot against velocity on a log-log scale (Fig.~\ref{fig:F-vs-U}). This will highlight any divergence in the drag force response from the classical, quadratic relationship (a horizontal line in this log-log plot) since the gradients of the lines will be equivalent to the Vogel exponent in Eq.~(\ref{eq:Vogel-exponent}).

\begin{figure*}[htb]
\centering
\includegraphics[width=0.8\paperwidth]{Fig-1_F-vs-U}
\caption{Speed-specific drag variation with velocity for: (a-b) \Alnus{}; (c-d) \Populus{}; and (e-f) \Salix specimens. Plots in the left and right columns represent foliated and defoliated states respectively. The thick dashed lines (\hdashrule[0.5ex]{0.875cm}{1pt}{1mm}) illustrate schematically the slope of a linear force-velocity relationship.}
\label{fig:F-vs-U}
\end{figure*}

From the data shown in Fig.~\ref{fig:F-vs-U}, it can be seen that, for all specimens, the relationship between drag force and velocity does indeed deviate away from the quadratic law described by the classical drag equation. Instead, the drag force response appears to exhibit a `stiff' regime at low velocities where for several specimens $\psi \approx 0$, and a `flexible' regime at higher velocities where $\psi \approx -1$. This confirms the unsuitability of representing flexible floodplain woodland as rigid cylinders with the unmodified classical drag equation.

The variation in the characteristic drag coefficient \CdAp can also be inferred from Fig.~\ref{fig:F-vs-U} as, according to the classical drag equation, $F / U^2 = \frac{1}{2} \rho C_d A_p$ and $\rho$ can be taken as constant over the range of temperatures in the towing tank. The reduction in characteristic drag coefficient with velocity is consistent with the Cauchy model proposed in Eq.~(\ref{eq:Cauchy-drag}), which states that $C_d A_p \propto U^{\psi}$. Comparing the initial characteristic drag coefficient responses, it is observed that the transition to a `flexible' regime happens at a lower velocity when a specimen is foliated (see Fig.~\ref{fig:F-vs-U}). This is because the extra surface area of the foliage creates a larger drag force for a given velocity, thus forcing the tree to start reconfiguring at a lower velocity than it would have if it was defoliated.

The testing of foliated and defoliated specimens allows the impact of foliage on the overall drag force to be investigated by considering the ratio of the total foliated drag force to the defoliated drag force, \ie $F_{fol} = (F_{tot} - F_{defol}) / F_{tot}$. This analysis showed that as the velocity increases, the percentage of the drag force caused by foliage rapidly decreases until it becomes relatively constant with velocity for a given specimen (Fig.~\ref{fig:Prop-foliage}). This suggests that the reconfiguration and streamlining of the foliage (but not necessarily the entire tree) mostly occurs below a certain threshold velocity (see also \citealp{Dittrich-12, Aberle-13}).

\begin{figure}[htb]
\centering
\includegraphics[width=0.95\columnwidth]{Fig-2_F-Fol}
\caption{Proportion of total drag force caused by foliage for each \Alnus (A), \Populus (P) and \Salix (S) specimen.}
\label{fig:Prop-foliage}
\end{figure}

From examination of where the effect of foliage begins to become constant with velocity in Fig.~\ref{fig:Prop-foliage}, the threshold velocity appears to range from 0.35~\mps to 0.75~\mps. This is in agreement with the findings of \citet{Vastila-13}, who observed that the reconfiguration of foliated \Populus \textit{nigra} branches was mostly complete at flow velocities of 0.6~\mps. At velocities below this threshold, the foliage is responsible for a significant proportion (40\% to 75\%) of the total drag force. At higher velocities, the contribution of foliage to the total drag force is reduced and ranges from 10~\% to 50~\%.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cauchy Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sec:Cauchy-results}

\noindent{}Following the approach in Section~\ref{sec:Cauchy-theory}, the `Cauchy model' according to Eq.~(\ref{eq:Cauchy-drag}) was applied to the force-velocity data of each specimen. A power-law regression analysis was used to obtain values for the coefficient $K$ and Vogel exponent \E. The coefficient of determination \Rsq was greater than 0.972 for all specimens, verifying the regression as a suitable approximation. For those specimens where flexural rigidity or volumetric data was not recorded, a $K$ value could not be calculated. However, a Vogel exponent could still be obtained in such cases by fitting a simple power-law expression similar to Eq.~(\ref{eq:Vogel-exponent}) to the force-velocity data.

Due to the experimental procedure, two flexural rigidity values were recorded for a given specimen: $EI_{25}$ and $EI_{50}$ (see Section~\ref{sec:Physical-properties}). It was therefore possible to calculate two separate $K$ values for each foliated or defoliated specimen. To avoid confusion, the $K$ values determined using $EI_{25}$ and $EI_{50}$ will henceforth be denoted $K_{25}$ and $K_{50}$, respectively.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% K Values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[htb]
\centering
\includegraphics[width=0.95\columnwidth]{Fig-3_Cauchy-K}
\caption{Comparison of $K_{25}$ and $K_{50}$ values for the \Alnus, \Populus and \Salix specimens.}
\label{fig:Cauchy-K}
\end{figure}

The resulting $K_{25}$ and $K_{50}$ values are shown in Fig.~\ref{fig:Cauchy-K} for all foliated and defoliated specimens. The average values for each species are presented in Tab.~\ref{tab:Average-K}. In all cases, both $K_{25}$ and $K_{50}$ were found to be larger in magnitude when a specimen was foliated. This can be attributed to the fact that the coefficient is related to the characteristic drag coefficient \CdAp. The added surface area of the foliage increases the total projected area, resulting in a corresponding increase in the initial characteristic drag coefficient and thus the value of $K$. It can be seen from Tab.~\ref{tab:Average-K} that, while the foliated \Alnus and \Salix specimens have similar $K$ values, the findings suggest that the values are species-specific.

\begin{table}[htb]
\caption{Average $K_{25}$ and $K_{50}$ values for each species. \textit{F} and \textit{D} indicate foliated and defoliated states respectively.}
\centering
\begin{footnotesize}
\begin{tabular}
{m{0.1\columnwidth}
>{\centering}m{0.1\columnwidth}
>{\centering}m{0.1\columnwidth}}
\toprule
Species & $K_{25}$ (m\sps{2}) & $K_{50}$ (m\sps{2}) \tabularnewline \midrule
\Alnus \textit{F} & 0.043 & 0.048 \tabularnewline
\Alnus \textit{D} & 0.034 & 0.036 \tabularnewline \addlinespace
\Populus \textit{F} & 0.022 & 0.024 \tabularnewline
\Populus \textit{D} & 0.017 & 0.018 \tabularnewline \addlinespace
\Salix \textit{F} & 0.047 & 0.047 \tabularnewline
\Salix \textit{D} & 0.024 & 0.024 \tabularnewline \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Average-K}
\end{table}

The relationship between $K$ and the specimens' projected area is highlighted by the points towards the top right and lower left of Fig.~\ref{fig:Cauchy-K}. The \Salix specimen \textit{S3}, which has the greatest $K$ values in both foliated and defoliated states, was also the heaviest and most voluminous tested specimen. It was also significantly stiffer than the other specimens (see Tab.~\ref{tab:Salix-elasticity}). Conversely, the \Salix branch \textit{S7B1} has the lowest $K$ values and was the second smallest specimen in terms of weight and volume. The smallest specimen did not have flexural rigidity data recorded and thus has no $K$ values for comparison.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vogel Exponent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Before discussing the Vogel exponent, it is noted that the terms `greater' and `lesser' in this context refer to the magnitude of the exponent, rather than its absolute value, since a more negative exponent implies a greater degree of reconfiguration. For instance, an exponent of $\psi = -0.75$ is said to be greater than an exponent of $\psi = -0.50$. The Vogel exponents and corresponding averages for each species are presented in Tab.~\ref{tab:Vogel-exp} and Fig.~\ref{fig:Vogel-E}. It should be noted that the full velocity range was used to determine \E due to the subjectivity of specifying a lower velocity threshold.

\begin{table}[htb]
\caption{Vogel exponent \E statistics for each species. \textit{F} and \textit{D} indicate foliated and defoliated states respectively.}
\centering
\begin{footnotesize}
\begin{tabular}
{m{0.1\columnwidth}
>{\centering}m{0.1\columnwidth}
>{\centering}m{0.1\columnwidth}
>{\centering}m{0.1\columnwidth}}
\toprule
Species & Min & Max & Average \tabularnewline \midrule
\Alnus \textit{F} & -0.613 & -0.811 & -0.727 \tabularnewline
\Alnus \textit{D} & -0.454 & -0.774 & -0.573 \tabularnewline \addlinespace
\Populus \textit{F} & -0.692 & -1.001 & -0.825 \tabularnewline
\Populus \textit{D} & -0.653 & -0.911 & -0.775 \tabularnewline \addlinespace
\Salix \textit{F} & -0.503 & -1.051 & -0.818 \tabularnewline
\Salix \textit{D} & -0.647 & -0.959 & -0.836 \tabularnewline \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Vogel-exp}
\end{table}

Examining the three species separately, it can be seen that the \Alnus specimens tend to have a Vogel exponent that is less negative than the other two species (Tab.~\ref{tab:Vogel-exp}). This behaviour is especially evident when the \Alnus specimens are in a defoliated state, with the exponents of the trees \textit{A3}, \textit{A4} and \textit{A5} accounting for three of the least negative exponents overall (see Fig.~\ref{fig:Vogel-E}). The results thus suggest that the \Alnus species is less flexible than the \Populus and \Salix specimens currently tested. However, the \Alnus specimens actually had the lowest (\ie most flexible) average $E_{25}$ and $E_{50}$ values of $3.64 \times 10^9$~\ymod and $6.43 \times 10^9$~\ymod, respectively. For comparison, the \Populus specimens had average values of $E_{25} = 5.94 \times 10^9$~\ymod and $E_{50} = 7.30 \times 10^9$~\ymod. The average modulus of elasticity values for the \Salix specimens are included in Tab.~\ref{tab:Salix-elasticity}.

This may seem contradictory, but could be due to branch morphology and the distribution of flexural rigidity over the entire tree. For example, while the \Alnus specimens had more flexible main stems ($EI_{25}$ and $EI_{50}$ values), their outer branches may have been thicker and thus less flexible than those of the \Populus and \Salix specimens. The mode of reconfiguration is also likely to differ between species, therefore further altering the Vogel exponent. Finally, the small sample sizes of the \Alnus and \Populus trees tested here make it difficult to draw general conclusions.

\begin{figure}[htb]
\centering
\includegraphics[width=0.95\columnwidth]{Fig-4_Vogel-E}
\caption{Individual and species-averaged Vogel exponents for the \Alnus (A), \Populus (P) and \Salix (S) specimens.}
\label{fig:Vogel-E}
\end{figure}

Regardless of these issues, the Vogel values calculated here are consistent with those reported in the literature. For example, \citet{Vastila-13} tested arrays of foliated \Populus \textit{nigra} branches in a tilting flume and evaluated an average Vogel exponent of $-1.03$ using the full velocity range (0.03~\mps to 0.61~\mps). This is close to the value of $\psi = -1.0$ for the foliated \Populus specimen \textit{P1}, although the average exponent for the foliated \Populus specimens in the present study is lower at $-0.825$ (see Tab.~\ref{tab:Vogel-exp}). The difference may be due to the fact that only one specimen at a time was tested in the current study, as opposed to the arrays of branches utilised in the aforementioned study. Further, the difference in scale (trees versus small branches) and range of testing velocities between the two studies could be responsible for some variation. \citet{Vastila-13} summarised additional exponent values from similar experiments \citep{Jarvela-04a, Jarvela-06b, Schoneboom-10} involving \Salix \textit{caprea} ($\psi = -0.57$), \Salix \textit{triandra x viminalis} ($\psi = -0.90$), and artificial branches ($\psi = -0.74$). These values are in good agreement with those presented here for the \Salix \textit{alba} specimens.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Physical Property Correlation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to quantify any relationships between the model parameters and the physical properties of the specimens, a further linear regression analysis was undertaken. Only the \Salix specimens were considered in this analysis due to the small sample sizes of the \Alnus and \Populus groups.

The analysis confirmed the close relationship between $K$ and the \Salix specimens' volume (Fig.~\ref{fig:K-vs-volume}). This was seen qualitatively in Fig.~\ref{fig:Cauchy-K} and is due to a larger tree having an increased projected area and thus a greater value for $K$ since it is related to the tree's initial, undisturbed characteristic drag coefficient \CdAp (Eq.~\ref{eq:Cauchy-drag}).

\begin{figure}[htb]
\centering
\includegraphics[width=0.95\columnwidth]{Fig-5_K-vs-Vol}
\caption{Correlation of $K$ with the \Salix specimens' foliated and defoliated volume.}
\label{fig:K-vs-volume}
\end{figure}

Similarly, there was good correlation between $K$ and the specimens' foliated wet mass ($R^2 > 0.90$). On the other hand, the correlation with wet mass was weaker when the specimens were defoliated ($R^2 > 0.73$), indicating that the leaf mass plays a significant role in drag creation, while only accounting for around 5\% to 30\% of total mass (Tab.~\ref{tab:Specimen-properties}).

There was found to be little to no correlation between the \Salix specimens' physical properties and the Vogel exponent \E, apart from a somewhat close fit with the foliated specimens' $EI_{50}$ values ($R^2 = 0.67$) and first-quartile diameter $d_{25}$ ($R^2 = 0.54$). The stronger correlation of the Vogel exponent with the specimens' mid-stem flexural rigidity $EI_{50}$ over the first-quartile flexural rigidity $EI_{25}$ ($R^2 = 0.52$) suggests that the $EI_{50}$ values may be a more accurate measure of a tree's overall stiffness.

The lack of correlation between the Vogel exponent and the physical properties recorded here for the \Salix specimens is unexpected, but is possibly related to the difficulty in classifying the reconfiguration of complex objects such as full-scale trees. For example, the differing flexural rigidities of the stem, branches and leaves are not accounted for in the current model. Any relationship between the Vogel exponent and a specimen's physical properties may also not be linear, as assumed here. However, these issues are recommended for future study as they are outside the scope of the present paper.

To check that the varying range of final testing velocities for each specimen was not responsible for introducing bias, Vogel exponents were re-calculated for each specimen using a common maximum velocity of 1.0~\mps. These were then re-correlated with the specimens' physical properties. The results of this second analysis were in agreement with the original analysis and in fact showed slightly less overall correlation. Therefore, the species-averaged Vogel exponents from the full velocity range (Tab.~\ref{tab:Vogel-exp}) are utilised in the following model predictions. This is consistent with the species-specific \E values found by \citet{Jarvela-04a} and \citet{Aberle-12}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model Predictions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Employing average Vogel exponents and the linear $K{-}V$ relationships (Fig.~\ref{fig:K-vs-volume}) to determine $K_{25}$ and $K_{50}$, the `Cauchy model' can be used to predict the measured drag force of the \Salix specimens with reasonable agreement (Fig.~\ref{fig:Model-predictions}). For the predictions based on the $EI_{25}$ and $K_{25}$ values, the average mean errors for the foliated and defoliated \Salix specimens were 47.5~\% and 42.7~\%, respectively. When using the $EI_{50}$ and $K_{50}$ values, the average mean errors were 45.8~\% (foliated) and 42.8~\% (defoliated).

\begin{figure}[htb]
\centering
\includegraphics[width=0.95\columnwidth]{Fig-6_Model-Results}
\caption{Drag force as predicted by the Cauchy model for the foliated and defoliated \Salix specimens. The relationships are based on the mid-stem flexural rigidity $EI_{50}$ values.}
\label{fig:Model-predictions}
\end{figure}

Although independent data were not available for validation, it can be seen from Fig.~\ref{fig:Model-predictions} that the model is relatively accurate over the broad range of towing velocities in this study. In addition, techniques have recently been developed which allow the geometry or leaf area index (LAI) of vegetation to be estimated via remote sensing \citep{Rautiainen-03, Zheng-09, Antonarakis-10, Forzieri-11}, which in turn could be used to estimate tree volume. Combined with average modulus of elasticity values and \insitu measurements of average tree height and stem diameter, it could thus provide a powerful tool for predicting the drag force response of floodplain woodland in flood conditions.