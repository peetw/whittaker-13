#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set term postscript eps enhanced monochrome dl 2 size 9cm,5.75cm font "NimbusRomNo9L-Regu,14" fontfile "/usr/share/texlive/texmf-dist/fonts/type1/public/txfonts/rtxmi.pfb"

DATA = "../data/cauchy-regr-EI25.csv"

set datafile separator ","

set output "../images/Fig-4_Vogel-E.eps"

set macro

unset grid
set border lw 0.25

set xtics rotate by -45 scale 0.75 font ",10"
set mytics 2

set xrange [1:36]
set yrange [-1.2:0]

set xlabel "Specimen" font ",18"
set ylabel "{/rtxmi \\040} (-)" font ",18"

set key Left reverse box lw 0.25 spacing 1.25 width -2


######################################################################################################################################
# GET AVERAGE VALUES
######################################################################################################################################

stats DATA every :::0::0 u 3 name "ALN_FOL" nooutput

stats DATA every :::0::0 u 6 name "ALN_DEFOL" nooutput

stats DATA every :::1::1 u 3 name "POP_FOL" nooutput

stats DATA every :::1::1 u 6 name "POP_DEFOL" nooutput

stats DATA every :::2::2 u 3 name "SAL_FOL" nooutput

stats DATA every :::2::2 u 6 name "SAL_DEFOL" nooutput


######################################################################################################################################
# PLOT
######################################################################################################################################

# Functions to space xtics and plot average Vogel exponents for each species
xspacing(x) = (x<7 ? x : (x<15 ? x+1 : x+2))
mean(x,min,max,val) = ( (x>=min && x<=max) ? val : (1/0) )

plot	DATA u (xspacing($0)):3:xtic(1) w points pt 6 t "Fol. exponent", \
		mean(x,1.5,6.5,ALN_FOL_mean) w lines lt 4 lw 1 t "Fol. average", \
		DATA u (xspacing($0)):6:xtic(1) w points pt 9 t "Defol. exponent", \
		mean(x,1.5,6.5,ALN_DEFOL_mean) w lines lt 2 lw 2 t "Defol. average", \
		mean(x,7.5,15.5,POP_FOL_mean) w lines lt 4 lw 1 notitle, \
		mean(x,7.5,15.5,POP_DEFOL_mean) w lines lt 2 lw 2 notitle, \
		mean(x,16.5,35.5,SAL_FOL_mean) w lines lt 4 lw 1 notitle, \
		mean(x,16.5,35.5,SAL_DEFOL_mean) w lines lt 2 lw 2 notitle
