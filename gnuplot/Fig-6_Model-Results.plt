#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set term postscript eps enhanced monochrome dl 3 size 9cm,5.75cm font "NimbusRomNo9L-Regu,14"

DATA = "../data/Cauchy-EI50_Salix.csv"

set datafile separator ","

set output "../images/Fig-6_Model-Results.eps"

set macro

unset grid
set border lw 0.25

set logscale xy

set xrange [0.1:1000]
set yrange [0.1:1000]

set format x "%g"
set format y "%g"

set xlabel "Measured, {/NimbusRomNo9L-ReguItal F} (N)" font ",17"
set ylabel "Predicted, {/NimbusRomNo9L-ReguItal F} (N)" font ",17"

set key top left Left reverse box lw 0.25 height 0.5 width -1.5

f(x) = x
key(x) = -1


plot	key(x) w points pt 6 t "Foliated", \
		for [i=2:146:8] DATA u i:(column(i+2)) w points pt 6 notitle, \
		key(x) w points pt 1 t "Defoliated", \
		for [i=3:147:8] DATA u i:(column(i+2)) w points pt 1 notitle, \
		f(x) w lines lt 2 t "1:1 agreement"
