#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set term postscript eps enhanced monochrome dl 3 size 9cm,5.75cm font "NimbusRomNo9L-Regu,14"

DATA = "../data/Cauchy_K.csv"

set datafile separator ","

set output "../images/Fig-3_Cauchy-K.eps"

set macro

unset grid
set border lw 0.25

set logscale xy

set xrange [0.001:0.3]
set yrange [0.001:0.3]

set xlabel "{/NimbusRomNo9L-ReguItal K}_{25} (m^{2})" font ",18"
set ylabel "{/NimbusRomNo9L-ReguItal K}_{50} (m^{2})" font ",18" offset 2.5,0

set xtics add ("0.3" 0.3)
set ytics add ("0.3" 0.3)

set key left Left reverse box lw 0.25 width -2 spacing 1.5

f(x) = x

set label 1 "S3" at 0.15,0.032
set arrow 1 from 0.16,0.04 to 0.1,0.065 lw 0.5
set arrow 2 from 0.16,0.04 to 0.2,0.15 lw 0.5

set label 2 "S7B1" at 0.004,0.03
set arrow 3 from 0.005,0.025 to 0.0068,0.0092 lw 0.5
set arrow 4 from 0.005,0.025 to 0.0105,0.0135 lw 0.5

plot	DATA u 2:3 w points pt 6 t "Foliated", \
		DATA u 4:5 w points pt 9 t "Defoliated", \
		f(x) w lines lt 2 t "1:1 agreement"
