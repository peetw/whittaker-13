#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set term postscript eps enhanced monochrome dl 3 size 9cm,5.75cm font "NimbusRomNo9L-Regu,14"

DATA_A = "../data/Cauchy_K.csv"
DATA_B = "../data/Physical_properties.csv"

set datafile separator ","

set output "../images/Fig-5_K-vs-Vol.eps"

set macro

unset grid
set border lw 0.25

#set xrange [0:6000]
set yrange [0:*]

set xlabel "{/NimbusRomNo9L-ReguItal V} (cm^3)" font ",18"
set ylabel "{/NimbusRomNo9L-ReguItal K} (m^2)" font ",18"

set key left Left reverse box lw 0.25 spacing 1.5 width -5 height 0.5


######################################################################################################################################
# MERGE K DATA AND PHYS PROP DATA
######################################################################################################################################

DATA = "tmp.csv"
TMP_A = "a.tmp"
TMP_B = "b.tmp"

!awk 'NR < 3 || NR > 17 {print $0","}' @DATA_A > @TMP_A
!awk 'NR < 3 || NR > 17 {print $0}' @DATA_B | cut -d , -f 2- > @TMP_B
!paste -d , @TMP_A @TMP_B > @DATA

!rm @TMP_A @TMP_B


######################################################################################################################################
# LINEAR REGRESSION
######################################################################################################################################

K25_fol(x) = m*x+c
fit K25_fol(x) DATA u 14:2 via m,c

K25_defol(x) = n*x+d
fit K25_defol(x) DATA u 19:4 via n,d

K50_fol(x) = o*x+e
fit K50_fol(x) DATA u 14:3 via o,e

K50_defol(x) = p*x+f
fit K50_defol(x) DATA u 19:5 via p,f

!rm fit.log


######################################################################################################################################
# PLOT GRAPH
######################################################################################################################################

dummy(x) = -1

plot	dummy(x) w linesp lt 1 pt 6 t "{/NimbusRomNo9L-ReguItal K}_{25} fol. ({/NimbusRomNo9L-ReguItal R}^2 = 0.93)", \
		DATA u 14:2 w points pt 6 notitle, \
		K25_fol(x) w lines lt 1 notitle, \
		dummy(x) w linesp lt 2 pt 4 t "{/NimbusRomNo9L-ReguItal K}_{50} fol. ({/NimbusRomNo9L-ReguItal R}^2 = 0.91)", \
		DATA u 14:3 w points pt 4 notitle, \
		K50_fol(x) w lines lt 2 notitle, \
		dummy(x) w linesp lt 3 pt 9 t "{/NimbusRomNo9L-ReguItal K}_{25} defol. ({/NimbusRomNo9L-ReguItal R}^2 = 0.82)", \
		DATA u 19:4 w points pt 9 notitle, \
		K25_defol(x) w lines lt 3 notitle, \
		dummy(x) w linesp lt 4 pt 11 t "{/NimbusRomNo9L-ReguItal K}_{50} defol. ({/NimbusRomNo9L-ReguItal R}^2 = 0.73)", \
		DATA u 19:5 w points pt 11 notitle, \
		K50_defol(x) w lines lt 4 notitle

!rm @DATA
