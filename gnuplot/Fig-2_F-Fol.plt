#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set term postscript eps enhanced monochrome dl 3 font "NimbusRomNo9L-Regu,18"

DATA = "../data/F-vs-U.csv"

set datafile separator ","

set output "../images/Fig-2_F-Fol.eps"

set macro

unset grid
set border lw 0.25

set xrange[0:*]
set yrange[0:100]

set xtics font ",20"
set ytics font ",20"

set format x "%g"
set format y "%g"

set xlabel "{/NimbusRomNo9L-ReguItal U} (ms^{-1})" font ",24"
set ylabel "{/NimbusRomNo9L-ReguItal F_{fol}} (%)" font ",24"

set key Left reverse box lw 0.5 font ",14" autotitle columnheader maxrows 9

# Specify styles
set pointsize 1.4
lines(n) = (n < 10 ? n = 2 : (n > 9 && n < 19 ? n = 4 : n = 0))
points(n) = (n > 9 ? points(n-9) : (n < 5 ? n : n*2 - 4))

# i set to Ratio rather than Velocity column so that NaN is properly skipped and lines are continuous
plot	for [i=4:4:5] DATA u (column(i-3)):i w linesp lt lines((i+1)/5) pt points((i+1)/5), \
		for [i=14:24:5] DATA u (column(i-3)):i w linesp lt lines((i-4)/5) pt points((i-4)/5), \
		for [i=39:49:5] DATA u (column(i-3)):i w linesp lt lines((i-14)/5) pt points((i-14)/5), \
		for [i=59:84:5] DATA u (column(i-3)):i w linesp lt lines((i-19)/5) pt points((i-19)/5), \
		for [i=94:99:5] DATA u (column(i-3)):i w linesp lt lines((i-24)/5) pt points((i-24)/5), \
		for [i=109:114:5] DATA u (column(i-3)):i w linesp lt lines((i-29)/5) pt points((i-29)/5), \
		for [i=124:159:5] DATA u (column(i-3)):i w linesp lt lines((i-34)/5) pt points((i-34)/5)
