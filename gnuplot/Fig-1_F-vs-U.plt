#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set term postscript eps enhanced monochrome dl 3 size 19cm,16cm font "NimbusRomNo9L-Regu,14" fontfile "/usr/share/texlive/texmf-dist/fonts/type1/public/txfonts/txsy.pfb"

DATA = "../data/F-vs-U.csv"

set datafile separator ","

set output "../images/Fig-1_F-vs-U.eps"

set macro

unset grid
set border lw 0.25

set logscale xy

set xrange[0.1:10]
set yrange[1:1000]

set xlabel "{/NimbusRomNo9L-ReguItal U} (ms^{-1})" font ",17"
set ylabel "{/NimbusRomNo9L-ReguItal F} / {/NimbusRomNo9L-ReguItal U}^2 (N / m^{2}s^{-2})" font ",17"

set xtics
set ytics

set format x "%g"
set format y "%g"

LW = 1

set linetype 1 lw LW pt 1
set linetype 2 lw LW pt 2
set linetype 3 lw LW pt 3
set linetype 4 lw LW pt 4
set linetype 5 lw LW pt 6
set linetype 6 lw LW pt 8
set linetype 7 lw LW pt 10
set linetype 8 lw LW pt 12
set linetype 9 lw LW pt 14
set linetype 10 lw LW pt 32

set multiplot layout 3,2

set bmargin 3.5
set tmargin 1

set lmargin 8.2
set rmargin 1.5

LT_LINEAR = 1
LW_LINEAR = 4

set key Left reverse box lw 0.5 autotitle columnheader height 0.75 width 1

set pointsize 1.35

######################################################################################################################################
# ALNUS
######################################################################################################################################

# Foliated
f(x) = (x < 0.4 || x > 1.9 ? 1/0 : 200/x)

plot	for [i=1:21:5] DATA u i:(column(i+1)/column(i)**2) w linesp, \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR


# Defoliated
f(x) = (x < 0.8 || x > 2.9 ? 1/0 : 90/x)

style(n) = (n+4)/5

plot	for [i=1:1] DATA u i:(column(i+2)/column(i)**2) w linesp lt style(i), \
		for [i=11:21:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt style(i), \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR


######################################################################################################################################
# POPULUS
######################################################################################################################################

# Foliated
f(x) = (x < 0.6 || x > 2.7 ? 1/0 : 170/x)

plot	for [i=26:61:5] DATA u i:(column(i+1)/column(i)**2) w linesp, \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR


# Defoliated
f(x) = (x < 1.0 || x > 2.8 ? 1/0 : 90/x)

style(n) = (n-21)/5

plot	for [i=36:46:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt style(i), \
		for [i=56:61:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt style(i), \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR


######################################################################################################################################
# SALIX
######################################################################################################################################

set key left bottom horizontal height 0.25

# Foliated
f(x) = (x < 0.6 || x > 4 ? 1/0 : 275/x)

lines(n) = (n < 10 ? n = 1 : n > 9 && n < 19 ? n = 3 : n = 0)
points(n) = (n < 5 ? n : (n < 10 ? n*2 - 4 : (n < 14 ? n-9 : (n == 19 ? 1 : (n-9)*2 - 4))))

plot	for [i=66:156:5] DATA u i:(column(i+1)/column(i)**2) w linesp lt lines((i-61)/5) pt points((i-61)/5), \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR


# Defoliated
set label "(a)" at screen 0.08, screen 0.966
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.26, screen 0.945
set label "(b)" at screen 0.58, screen 0.966
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.82, screen 0.89
set label "(c)" at screen 0.08, screen 0.633
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.295, screen 0.59
set label "(d)" at screen 0.58, screen 0.633
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.825, screen 0.555
set label "(e)" at screen 0.455, screen 0.3
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.32, screen 0.27
set label "(f)" at screen 0.955, screen 0.3
set label "{/NimbusRomNo9L-ReguItal F} {/txsy \\057} {/NimbusRomNo9L-ReguItal U}" at screen 0.85, screen 0.255

set label "See prev. plot" at screen 0.58, screen 0.08
set object rectangle lw 1.4 from screen 0.575, screen 0.0665 to screen 0.6575, screen 0.092

unset key

f(x) = (x < 0.8 || x > 6 ? 1/0 : 275/x)

lines(n) = (n < 8 ? n = 1 : n > 7 && n < 16 ? n = 3 : n = 0)
points(n) = (n < 5 ? n : (n > 4 && n < 7 ? n*2-2 : (n == 7 ? 14 : (n == 8 ? 1 : (n == 9 ? 3 : (n == 16 ? 1 : (2*n-16)))))))

plot	for [i=66:81:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt lines((i-61)/5) pt points((i-61)/5), \
		for [i=91:96:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt lines((i-66)/5) pt points((i-66)/5), \
		for [i=106:111:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt lines((i-71)/5) pt points((i-71)/5), \
		for [i=121:156:5] DATA u i:(column(i+2)/column(i)**2) w linesp lt lines((i-76)/5) pt points((i-76)/5), \
		f(x) w lines lt LT_LINEAR lw LW_LINEAR
